> 分享作弊条, 用以快速组织演讲/讨论/展示/...

应用示例:[[CST] camp2py 破冰现场 + wow 预约访谈 的 cheetsheet - ofey404](https://gitlab.com/101camp/2py/tasks/issues/40)


## Meta
> 元数据: pre 的时间,地点,时长,目的与要求,备忘...


## Goal
> pre 目标,要讲清楚的问题,想要达到的效果...


## Timeline
> 按时间顺序排列的重点流程,预估用时,以及 tips...


## Draft
> 讲演时参照的稿子,细节,punchlines...


## Reference
> 过程中参考过的重要文章/图书/模块/代码/...

永远的: [如何提问](https://gitlab.com/101camp/2py/tasks/wikis/HandBooks/Hb4Ask)



## Logging
> 用倒序日期排列来从旧到新记要关键变化

- 190728 ofey404 init
    + ZQ 部署
